﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CqingWo.Core;

namespace CqingWo.Data
{
    /// <summary>
    /// 设备Dao
    /// </summary>
    public class DeviceDao
    {

        private static DeviceDao _deviceDao = null;

        private static readonly object Locker = new object();

        /// <summary>
        /// 单例
        /// </summary>
        /// <returns></returns>
        public static DeviceDao Instance
        {
            get
            {
                lock (Locker)
                {
                    return _deviceDao ?? (_deviceDao = new DeviceDao());
                }
            }
        }

        /// <summary>
        /// 设备列表
        /// </summary>
        /// <param name="pageSize">条数</param>
        /// <param name="pageNumber">当前页数</param>
        /// <returns></returns>
        public List<DeviceInfo> List(int pageSize, int pageNumber)
        {
            return CWMData.RDBS.DeviceList(pageSize, pageNumber);

        }


        /// <summary>
        /// 查询设备信息
        /// </summary>
        /// <param name="deviceSN">设备编号</param>
        /// <returns></returns>
        public DeviceInfo Show(string deviceSN)
        {
            return CWMData.RDBS.DeviceShow(deviceSN);
        }
        


    }
}
