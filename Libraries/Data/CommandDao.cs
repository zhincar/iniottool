﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CqingWo.Core;

namespace CqingWo.Data
{
    public class CommandDao
    {
        private static CommandDao _commanddao = null;

        public static readonly object Locker = new object();


        /// <summary>
        /// 单例
        /// </summary>
        /// <returns></returns>
        public static CommandDao Instance
        {
            get
            {
                lock (Locker)
                {
                    return _commanddao ?? (_commanddao = new CommandDao());
                }
            }
        }


        /// <summary>
        /// 发送命令
        /// </summary>
        /// <param name="command">命令</param>
        /// <param name="deviceList">设备列表</param>
        public bool Send(string command, List<string> deviceList)
        {
            

            return CWMData.RDBS.SendCommand(command, deviceList);

        }


    }
}
