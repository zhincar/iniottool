﻿using System;
using CqingWo.Core;
using Newtonsoft.Json;

namespace CqingWo.Data
{
    public class UserDao : DataService
    {

        private static UserDao _userDao = null;

        private static readonly object Locker = new object();

        /// <summary>
        /// 单例
        /// </summary>
        /// <returns></returns>
        public static UserDao Instance
        {
            get
            {
                lock (Locker)
                {
                    return _userDao ?? (_userDao = new UserDao());
                }
            }
        }



        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="account">账号/邮箱/手机</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        public void Login(string account, string password)
        {
            CWMData.RDBS.Login(account, password);
        }
    }
}
