﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CqingWo.Core;

namespace CqingWo.Data
{
    /// <summary>
    /// 消息Dao
    /// </summary>
    public class MessageDao
    {

        private static MessageDao _deviceDao = null;

        private static readonly object Locker = new object();

        /// <summary>
        /// 单例
        /// </summary>
        /// <returns></returns>
        public static MessageDao Instance
        {
            get
            {
                lock (Locker)
                {
                    return _deviceDao ?? (_deviceDao = new MessageDao());
                }
            }
        }


        /// <summary>
        /// 设备列表
        /// </summary>
        /// <param name="pageSize">条数</param>
        /// <param name="pageNumber">当前页数</param>
        /// <param name="startTime">开始时间</param>
        /// <param name="startMsgId">开始消息Id</param>
        /// <param name="deviceList">设备编号列表</param>
        /// <returns></returns>
        public List<Message2Info> List(int pageSize, int pageNumber, int startTime, int startMsgId, List<string> deviceList)
        {
            return CWMData.RDBS.MessageList(pageSize, pageNumber, startTime, startMsgId, deviceList);
        }

        /// <summary>
        /// 查询设备信息
        /// </summary>
        /// <param name="msgId">设备编号</param>
        /// <returns></returns>
        public void Show(int msgId)
        {
             CWMData.RDBS.MessageShow(msgId);
        }




    }
}
