﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CqingWo.Core;
using CqingWo.Data;

namespace CqingWo.Services
{
    public class Commands
    {

        private static Commands _commands = null;

        private static readonly object Locker = new object();

        /// <summary>
        /// 单例
        /// </summary>
        public static Commands Instance
        {
            get
            {
                lock (Locker)
                {
                    return _commands ?? (_commands = new Commands());
                }
            }
        }


        public bool Send(string command, List<string> deviceList)
        {

            try
            {
                return CommandDao.Instance.Send(command, deviceList);
            }
            catch (Exception ex)
            {
                Logs.CWMLogs.WriteLog("发送命令", ex);
            }

            return false;

        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="command"></param>
        /// <param name="deviceInfoList"></param>
        public bool Send(string command, ObservableCollection<DeviceInfo> deviceInfoList)
        {

            try
            {
                List<string> deviceList = new List<string>();

                foreach (DeviceInfo info in deviceInfoList)
                {
                    deviceList.Add(info.DeviceSN);
                }

                return Send(command, deviceList);


            }
            catch (Exception ex)
            {
                Logs.CWMLogs.WriteLog("发送命令", ex);
            }
            return false;
        }
    }
}
