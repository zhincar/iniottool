﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CqingWo.Services
{
    public class User32Utils
    {



        /// <summary>
        /// 发起消息
        /// </summary>
        /// <param name="hWnd">句柄</param>
        /// <param name="msg">消息</param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int msg, int wParam, int lParam);


        /// <summary>
        ///查询窗口
        /// </summary>
        /// <param name="lpClassName">类名</param>
        /// <param name="lpWindowName">窗口名称</param>
        /// <returns></returns>
        [DllImport("User32.dll", EntryPoint = "FindWindow")]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);


        /// <summary>
        /// 捕获异常
        /// </summary>
        /// <param name="hwnd">句柄</param>
        /// <param name="hwndChild"子窗口></param>
        /// <param name="lpClassName">类名</param>
        /// <param name="lpWindowName">窗口名称</param>
        /// <returns></returns>
        [DllImport("User32.dll", EntryPoint = "FindEx")]
        public static extern IntPtr FindEx(IntPtr hwnd, IntPtr hwndChild, string lpClassName, string lpWindowName);


    }
}
