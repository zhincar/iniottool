﻿using System;
using System.Threading.Tasks;
using CqingWo.Data;


namespace CqingWo.Services
{

    /// <summary>
    /// 用户操作类
    /// </summary>
    public class Users
    {
        private static Users _users = null;

        private Users()
        {
        }

        private static readonly object Locker = new object();

        /// <summary>
        /// 单例
        /// </summary>
        /// <returns></returns>
        public static Users Instance
        {
            get
            {
                lock (Locker)
                {
                    return _users ?? (_users = new Users());
                }
            }
        }

     

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="account">账号/邮箱/手机</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        public void Login(string account, string password)
        {
    
            CqingWo.Data.UserDao.Instance.Login(account, password);
    
        }
    }
}
