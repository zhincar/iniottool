﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using CqingWo.Core;
using CqingWo.Data;

namespace CqingWo.Services
{

    /// <summary>
    /// 消息
    /// </summary>
    public class Messages
    {
        private static Messages _messages = null;

        private static readonly object Locker = new object();

        private Messages()
        {
        }


        /// <summary>
        /// 单例
        /// </summary>
        /// <returns></returns>
        public static Messages Instance
        {
            get
            {
                lock (Locker)
                {
                    return _messages ?? (_messages = new Messages());
                }
            }
        }


        /// <summary>
        /// 消息列表
        /// </summary>
        /// <param name="pageSize">条数</param>
        /// <param name="pageNumber">当前页数</param>
        /// <param name="startTime">开始时间</param>
        /// <param name="startMsgId">开始消息Id</param>
        /// <param name="deviceList">设备编号列表</param>
        /// <returns></returns>
        public List<Message2Info> List(int pageSize, int pageNumber, int startTime, int startMsgId, List<string> deviceList)
        {

            List<Message2Info> message2InfoList = new List<Message2Info>();

            try
            {

                message2InfoList = MessageDao.Instance.List(pageSize, pageNumber, startTime, startMsgId, deviceList);
            }
            catch (Exception ex)
            {
                Logs.CWMLogs.WriteLog("设备列表", ex);

            }

            return message2InfoList;
        }

        /// <summary>
        /// 查询设备消息
        /// </summary>
        /// <param name="msgId">消息编号</param>
        /// <returns></returns>
        public void Show(int msgId)
        {

            try
            {
                MessageDao.Instance.Show(msgId);
            }

            catch (CWMMessageSuccessExcption)
            {
                Logs.CWMLogs.WriteLog("查询设备信息成功");
                throw;
            }
            catch (Exception ex)
            {
                Logs.CWMLogs.WriteLog("查询设备信息", ex);
            }
            
        }

        /// <summary>
        /// 消息列表
        /// </summary>
        /// <param name="pageSize">条数</param>
        /// <param name="pageNumber">当前页数</param>
        /// <param name="startTime">开始时间</param>
        /// <param name="startMsgId">开始消息Id</param>
        /// <param name="deviceInfoList">设备列表</param>
        /// <returns></returns>
        public List<Message2Info> List(int pageSize, int pageNumber, int startTime, int startMsgId, ObservableCollection<DeviceInfo> deviceInfoList)
        {
            if (deviceInfoList == null || deviceInfoList.Count <= 0)
            {
                return new List<Message2Info>();

            }

            List<string> deviceList = new List<string>();

            foreach (DeviceInfo deviceInfo in deviceInfoList)
            {
                if (string.IsNullOrWhiteSpace(deviceInfo.DeviceSN))
                {
                    continue;

                }

                

                deviceList.Add(deviceInfo.DeviceSN);

            }

            return List(pageSize, pageNumber, startTime, startMsgId, deviceList);
        }


    }
}
