﻿using System;
using System.Collections.Generic;
using System.Net;
using CqingWo.Core;


namespace CqingWo.Services
{
    public class CWMUtils
    {

        private static CWMUtils _cwmutils = null;

        private static readonly object Locker = new object();



        private CWMUtils()
        {


        }

        /// <summary>
        /// 单例
        /// </summary>
        /// <returns></returns>
        public static CWMUtils Instance
        {
            get
            {
                lock (Locker)
                {
                    return _cwmutils ?? (_cwmutils = new CWMUtils());
                }
            }
        }


        /// <summary>
        /// get
        /// </summary>
        /// <param name="methodUrl">地址</param>
        /// <returns></returns>
        public string DoGet(String methodUrl)
        {
            return DoGet(methodUrl, null);
        }


        /// <summary>
        /// get
        /// </summary>
        /// <param name="methodUrl">地址</param>
        /// <param name="parameters">参数</param>
        /// <returns></returns>
        public string DoGet(string methodUrl, Dictionary<string, string> parameters)
        {
            String httpUrl = GetApiUrl(methodUrl);

            HttpWebResponse response = HttpHelper.DoGetHttpResponse(httpUrl, parameters, GetApiHeader());


            if (response.StatusCode == HttpStatusCode.OK)
            {

                return HttpHelper.GetResponseString(response);

            }

            throw new CWMNetworkException("网络异常");

        }


        /// <summary>
        /// Post
        /// </summary>
        /// <param name="methodUrl">地址</param>
        /// <returns></returns>
        public string DoPost(string methodUrl)
        {
            return DoPost(methodUrl, null);
        }

        /// <summary>
        /// Post
        /// </summary>
        /// <param name="methodUrl">地址</param>
        /// <param name="parameters">参数</param>
        /// <returns></returns>
        private string DoPost(string methodUrl, Dictionary<string, object> parameters)
        {
            String httpUrl = GetApiUrl(methodUrl);

            HttpWebResponse response = HttpHelper.DoPostHttpResponse(httpUrl, parameters, GetApiHeader());

            if (response.StatusCode == HttpStatusCode.OK)
            {

                return HttpHelper.GetResponseString(response);

            }


            throw new CWMNetworkException("网络异常");
        }


        /// <summary>
        /// 拼装API URL
        /// </summary>
        /// <param name="methodUrl">地址</param>
        /// <returns></returns>
        private String GetApiUrl(String methodUrl)
        {
            return ConfigUtlis.GetInstance().GetAppConfigInfo().HttpUrl + methodUrl;
        }

        /// <summary>
        /// 拼装头文件
        /// </summary>
        /// <returns></returns>
        private IDictionary<string, string> GetApiHeader()
        {

            IDictionary<String, String> headers = new Dictionary<string, string>();

            headers.Add("X-CWMAPI-ApiKey", ConfigUtlis.GetInstance().GetAppConfigInfo().ApiKey);
            headers.Add("X-CWMAPI-ApiSecret", ConfigUtlis.GetInstance().GetAppConfigInfo().ApiSecret);

            return headers;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetProjectRootPath()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory;
            string rootpath = path.Substring(0, path.LastIndexOf("\\", StringComparison.Ordinal));
            //rootpath = rootpath.Substring(0, rootpath.LastIndexOf("\\", StringComparison.Ordinal));
            //rootpath = rootpath.Substring(0, rootpath.LastIndexOf("\\", StringComparison.Ordinal));
            return rootpath;
        }


        public bool SetCookie()
        {




            return false;

        }



    }
}
