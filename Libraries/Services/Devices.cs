﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CqingWo.Core;
using CqingWo.Data;

namespace CqingWo.Services
{
    public class Devices
    {

        private static Devices _devices = null;

        private static readonly object Locker = new object();

        private Devices()
        {
        }

        /// <summary>
        /// 单例
        /// </summary>
        /// <returns></returns>
        public static Devices Instance
        {
            get
            {
                lock (Locker)
                {
                    return _devices ?? (_devices = new Devices());
                }
            }
        }


        /// <summary>
        /// 设备列表
        /// </summary>
        /// <param name="pageSize">条数</param>
        /// <param name="pageNumber">当前页数</param>
        /// <returns></returns>
        public List<DeviceInfo> List(int pageSize, int pageNumber)
        {
            return DeviceDao.Instance.List(pageSize, pageNumber);

        }


        /// <summary>
        /// 查询设备信息
        /// </summary>
        /// <param name="deviceSN">设备编号</param>
        /// <returns></returns>
        public DeviceInfo Show(string deviceSN)
        {
            try
            {
                return DeviceDao.Instance.Show(deviceSN);
            }
            catch (Exception ex)
            {
                Logs.CWMLogs.WriteLog("查询设备信息", ex);
                return null;
            }
        }

    }
}
