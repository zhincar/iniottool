﻿using System;
using System.IO;

namespace CqingWo.Core
{

    /// <summary>
    /// 数据
    /// </summary>
    public class CWMData
    {


        private static IRdbsStrategy _irdbsstrategy = null;//关系型数据库策略

        private static object _locker = new object();//锁对象

        static CWMData()
        {
            try
            {
                string[] fileNameList = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "CqingWo.RDBSStrategy.*.dll", SearchOption.TopDirectoryOnly);
                _irdbsstrategy = (IRdbsStrategy)Activator.CreateInstance(Type.GetType(string.Format("CqingWo.RDBSStrategy.{0}.RDBSStrategy, CqingWo.RDBSStrategy.{0}", fileNameList[0].Substring(fileNameList[0].LastIndexOf("RDBSStrategy.") + 13).Replace(".dll", "")),
                                                                                            false,
                                                                                            true));
            }
            catch
            {
                throw new CWMException("创建'关系数据库策略对象'失败,可能存在的原因:未将'关系数据库策略程序集'添加到bin目录中;'关系数据库策略程序集'文件名不符合'CqingWo.RDBSStrategy.{策略名称}.dll'格式");
            }

        }

        /// <summary>
        /// 关系型数据库
        /// </summary>
        public static IRdbsStrategy RDBS
        {
            get { return _irdbsstrategy; }
        }

    }
}
