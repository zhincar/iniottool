﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CqingWo.Core
{
    public partial interface IRdbsStrategy
    {

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="command">账号/邮箱/手机</param>
        /// <param name="deviceList">设备编号列表</param>
        /// <returns></returns>
        bool SendCommand(string command, List<string> deviceList);


    }
}
