﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CqingWo.Core
{
    public partial interface IRdbsStrategy
    {

        /// <summary>
        /// 设备列表
        /// </summary>
        /// <param name="pageSize">条数</param>
        /// <param name="pageNumber">当前页数</param>
        /// <returns></returns>
        List<DeviceInfo> DeviceList(int pageSize, int pageNumber);


        /// <summary>
        /// 查询设备信息
        /// </summary>
        /// <param name="deviceSN">设备编号</param>
        /// <returns></returns>
        DeviceInfo DeviceShow(string deviceSN);
    }
}
