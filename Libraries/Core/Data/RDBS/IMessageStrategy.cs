﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CqingWo.Core
{
    public partial interface IRdbsStrategy
    {
        /// <summary>
        /// 消息列表
        /// </summary>
        /// <param name="pageSize">读取条数</param>
        /// <param name="pageNumber">当前页数</param>
        /// <param name="startTime">开始时间</param>
        /// <param name="startMsgId">开始消息Id</param>
        /// <param name="deviceList">设备编号列表</param>
        /// <returns></returns>
        List<Message2Info> MessageList(int pageSize, int pageNumber, int startTime, int startMsgId, List<string> deviceList);


        /// <summary>
        /// 消息详情
        /// </summary>
        /// <param name="msgId">消息编号</param>
        /// <returns></returns>
        void MessageShow(int msgId);

    }
}
