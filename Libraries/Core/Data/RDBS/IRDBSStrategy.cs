using System;
using System.Data.Common;

namespace CqingWo.Core
{
    /// <summary>
    /// 系数据库策略之数据库分部接口
    /// </summary>
    public partial interface IRdbsStrategy
    {
   
        /// <summary>
        /// 运行SQL语句
        /// </summary>
        /// <param name="sql">SQL语句</param>
        /// <returns></returns>
        string RunSql(string sql);
    }
}
