﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CqingWo.Core
{
    public partial interface IRdbsStrategy
    {
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="account">账号/邮箱/手机</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        void Login(string account, string password);
    }
}
