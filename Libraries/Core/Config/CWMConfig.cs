﻿using System;
using System.IO;

namespace CqingWo.Core
{
    /// <summary>
    /// 青沃配置管理类
    /// </summary>
    public partial class CWMConfig
    {
        private static object _locker = new object();//锁对象

        private static IConfigStrategy _iconfigstrategy = null;//配置策略

        static CWMConfig()
        {
            try
            {



                string[] fileNameList = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "CqingWo.ConfigStrategy.*.dll", SearchOption.TopDirectoryOnly);

                foreach (string s in fileNameList)
                {
                    Logs.CWMLogs.WriteLog("fileNameList:" + s);

                }

                _iconfigstrategy = (IConfigStrategy)Activator.CreateInstance(Type.GetType(string.Format("CqingWo.ConfigStrategy.{0}.ConfigStrategy, CqingWo.ConfigStrategy.{0}", fileNameList[0].Substring(fileNameList[0].LastIndexOf("ConfigStrategy.") + 15).Replace(".dll", "")),
                                                                                         false,
                                                                                        true));
            }
            catch
            {
                throw new CWMException("创建'配置策略对象'失败,可能存在的原因:未将'配置策略程序集'添加到bin目录中;'配置策略程序集'文件名不符合'CqingWo.ConfigStrategy.{策略名称}.dll'格式");
            }
        }



        /// <summary>
        /// 获取config策略
        /// </summary>
        /// <returns></returns>
        public static IConfigStrategy Config
        {
            get { return _iconfigstrategy; }
        }


    }
}
