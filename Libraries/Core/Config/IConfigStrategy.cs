﻿using System;
using System.Collections.Generic;

namespace CqingWo.Core
{
    /// <summary>
    /// 青沃商城配置策略接口
    /// </summary>
    public partial interface IConfigStrategy
    {

        /// <summary>
        /// 保存参数
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">值</param>
        bool Save(string key, string value);


        /// <summary>
        /// 保存多个参数
        /// </summary>
        /// <param name="params2">参数</param>
        /// <returns></returns>
        bool Save(IDictionary<string, string> params2);


        /// <summary>
        /// 读取键值
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="defaultValue">默认值</param>
        string Read(string key, string defaultValue = "");


    }
}
