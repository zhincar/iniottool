﻿using System;
using System.Collections.Generic;
using CqingWo.Core;
using CqingWo.Logs;

namespace CqingWo.Services
{
    public class ConfigUtlis
    {

        private static ConfigUtlis _configUtlis = null;

        private static readonly object Locker = new object();


        /// <summary>
        /// AppName
        /// </summary>
        private AppConfigInfo _appConfigInfo;


        /// <summary>
        /// 单例
        /// </summary>
        /// <returns></returns>
        public static ConfigUtlis GetInstance()
        {
            lock (Locker)
            {
                return _configUtlis ?? (_configUtlis = new ConfigUtlis());
            }
        }

        /// <summary>
        /// 构造方法
        /// </summary>
        private ConfigUtlis()
        {


            _appConfigInfo = new AppConfigInfo();

       
            _appConfigInfo.Name = CWMConfig.Config.Read("Name", "物联英卡产品智能制造管理平台");
            _appConfigInfo.ApiKey = CWMConfig.Config.Read("ApiKey", "");
            _appConfigInfo.ApiSecret = CWMConfig.Config.Read("ApiSecret", "");
            _appConfigInfo.HttpUrl = CWMConfig.Config.Read("HttpUrl", "");






        }


        /// <summary>
        /// 获取配置信息
        /// </summary>
        /// <returns></returns>
        public AppConfigInfo GetAppConfigInfo()
        {
            return _appConfigInfo;
        }


        /// <summary>
        /// 保存APP设置
        /// </summary>
        /// <param name="configInfo">配置信息</param>
        /// <returns></returns>
        public bool SaveAppConfigInfo(AppConfigInfo configInfo)
        {

            try
            {

                IDictionary<string, string> params2 = new Dictionary<string, string>();
                params2.Add("ApiKey", configInfo.ApiKey);
                params2.Add("ApiSecret", configInfo.ApiSecret);
                params2.Add("HttpUrl", configInfo.HttpUrl);

                if (CWMConfig.Config.Save(params2))
                {
                    this._appConfigInfo = configInfo;
                    return true;
                }
            }
            catch (Exception)
            {
                CWMLogs.WriteLog("保存APP设置失败");
            }

            return false;

        }



    }
}
