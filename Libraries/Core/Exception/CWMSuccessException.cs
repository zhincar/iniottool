﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using CqingWo.Logs;

namespace CqingWo.Core
{
    public class CWMSuccessException : ApplicationException
    {


        public CWMSuccessException() { }

        public CWMSuccessException(string message) : base(message)
        {
            CWMLogs.WriteLog(message);
        }

        public CWMSuccessException(string message, System.Exception inner) : base(message, inner) { }

        public CWMSuccessException(SerializationInfo info, StreamingContext context) : base(info, context) { }



    }
}