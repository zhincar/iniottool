﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using CqingWo.Core.Domain.Message;
using CqingWo.Logs;

namespace CqingWo.Core
{
    public class CWMMessageSuccessExcption : CWMException
    {
        /// <summary>
        ///消息id
        /// </summary>
        public int MsgId { get; set; } = 0;

        /// <summary>
        /// 消息
        /// </summary>
        public Message2Info MessageInfo { get; set; }

        /// <summary>
        /// 解析记录
        /// </summary>
        public List<MessageRecordInfo> RecordInfoList { get; set; }

        public CWMMessageSuccessExcption() { }


        public CWMMessageSuccessExcption(string message)
            : base(message)
        {
            CWMLogs.WriteLog(message);
        }

        public CWMMessageSuccessExcption(int msgId, Message2Info messageInfo, List<MessageRecordInfo> recordInfoList, string message) : base(message)
        {
            MsgId = msgId;
            MessageInfo = messageInfo;
            RecordInfoList = recordInfoList;
        }



        public CWMMessageSuccessExcption(string message, System.Exception inner) : base(message, inner) { }

        public CWMMessageSuccessExcption(SerializationInfo info, StreamingContext context) : base(info, context) { }

    }
}