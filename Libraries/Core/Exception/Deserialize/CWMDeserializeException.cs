﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using CqingWo.Logs;

namespace CqingWo.Core
{
    public class CWMDeserializeException : CWMException
    {

        public CWMDeserializeException() { }

        public CWMDeserializeException(string message) : base(message)
        {
            CWMLogs.WriteLog(message);
        }

        public CWMDeserializeException(string message, System.Exception inner) : base(message, inner) { }

        public CWMDeserializeException(SerializationInfo info, StreamingContext context) : base(info, context) { }

    }
}
