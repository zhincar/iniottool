﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using CqingWo.Logs;

namespace CqingWo.Core
{
    public class CWMLoginSuccessExcption : CWMException
    {

        public AuthUserInfo AuthUserInfo { get; set; }

        public CWMLoginSuccessExcption() { }


        public CWMLoginSuccessExcption(string message)
            : base(message)
        {
            CWMLogs.WriteLog(message);
        }


        public CWMLoginSuccessExcption(AuthUserInfo authUserInfo, string message) : base(message)
        {
            AuthUserInfo = authUserInfo;
        }


        public CWMLoginSuccessExcption(string message, System.Exception inner) : base(message, inner) { }

        public CWMLoginSuccessExcption(SerializationInfo info, StreamingContext context) : base(info, context) { }

    }
}