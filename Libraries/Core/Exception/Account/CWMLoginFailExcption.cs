﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using CqingWo.Logs;

namespace CqingWo.Core
{
    public class CWMLoginFailExcption : CWMException
    {

        public CWMLoginFailExcption() { }

        public CWMLoginFailExcption(string message)
            : base(message)
        {
            CWMLogs.WriteLog(message);
        }

        public CWMLoginFailExcption(string message, System.Exception inner) : base(message, inner) { }

        public CWMLoginFailExcption(SerializationInfo info, StreamingContext context) : base(info, context) { }

    }
}