﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using CqingWo.Logs;

namespace CqingWo.Core
{
    public class CWMException : ApplicationException
    {


        public CWMException() { }

        public CWMException(string message) : base(message)
        {
            CWMLogs.WriteLog(message);
        }

        public CWMException(string message, System.Exception inner) : base(message, inner) { }

        public CWMException(SerializationInfo info, StreamingContext context) : base(info, context) { }



    }
}