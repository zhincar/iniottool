﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using CqingWo.Logs;

namespace CqingWo.Core
{
    public class CWMFailException : ApplicationException
    {


        public CWMFailException() { }

        public CWMFailException(string message) : base(message)
        {
            CWMLogs.WriteLog(message);
        }

        public CWMFailException(string message, System.Exception inner) : base(message, inner) { }

        public CWMFailException(SerializationInfo info, StreamingContext context) : base(info, context) { }



    }
}