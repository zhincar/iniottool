﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CqingWo.Core
{

    /// <summary>
    /// 消息模型
    /// </summary>
    [Serializable]
    public class MessageInfo
    {
        /**
     * 消息分类
     */
        public int Type { get; set; }



        /**
         * 消息状态
         */
        public int State { get; set; }


        /**
         * 消息说明
         */
        public string Message { get; set; }


        /**
         * 消息正文
         */
        public string Content { get; set; }

    }
}
