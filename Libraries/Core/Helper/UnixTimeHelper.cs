﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CqingWo.Core
{
    public class UnixTimeHelper
    {

        /// <summary>
        /// 获取unix时间戳格式
        /// </summary>
        /// <returns></returns>
        public static int GetUnixTimeStamp()
        {
            try
            {

                TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);

                return Convert.ToInt32(ts.TotalSeconds);
            }
            catch (Exception)
            {
                // ignored
            }


            return 0;

        }


        /// <summary>
        /// 将时间戳转换为日期类型，并格式化
        /// </summary>
        /// <param name="longDateTime"></param>
        /// <returns></returns>
        public static DateTime UnixTimeStampToDateTime(int longDateTime)
        {

            try
            {
                DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime date = start.AddSeconds(longDateTime).ToLocalTime();

                return date;
            }
            catch (Exception)
            {
                // ignored
            }

            return new DateTime();


        }


    }
}
