﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;


namespace CqingWo.Core
{
    public class HttpHelper
    {




        #region get/post原始方法


        /// <summary>
        /// 发送http post请求
        /// </summary>
        /// <param name="url">地址</param>
        /// <param name="parameters">查询参数集合</param>
        /// <param name="headers">头</param>
        /// <returns></returns>
        public static HttpWebResponse DoPostHttpResponse(string url, IDictionary<string, object> parameters, IDictionary<String, String> headers)
        {

            int httpStatusCode = 200;

            try
            {
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest; //创建请求对象



                if (request == null)
                {
                    throw new CWMNetworkException("网络请求发起失败");
                }

                request.Method = "POST"; //请求方式
                request.ContentType = "application/x-www-form-urlencoded"; //链接类型

                if (!(headers == null || headers.Count == 0))
                {

                    foreach (string key in headers.Keys)
                    {
                        request.Headers[key] = headers[key];
                    }

                }

                //构造查询字符串
                if (!(parameters == null || parameters.Count == 0))
                {
                    StringBuilder buffer = new StringBuilder();
                    bool first = true;
                    foreach (string key in parameters.Keys)
                    {

                        if (!first)
                        {
                            buffer.AppendFormat("&{0}={1}", key, parameters[key]);
                        }
                        else
                        {
                            buffer.AppendFormat("{0}={1}", key, parameters[key]);
                            first = false;
                        }
                    }


                    byte[] data = Encoding.UTF8.GetBytes(buffer.ToString());


                    //写入请求流
                    using (Stream stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }


                }


                HttpWebResponse rsp = request.GetResponse() as HttpWebResponse;

                if (rsp == null)
                {
                    throw new CWMNetworkException("网络请求异常");
                }

                httpStatusCode = (int)rsp.StatusCode;

                if (httpStatusCode / 100 == 2)
                {
                    return rsp;
                }

                throw new CWMNetworkException("网络请求状态异常");

            }
            catch (Exception ex)
            {
                Logs.CWMLogs.WriteLog("网络请求异常", ex);

                throw new CWMNetworkException(ex.Message);
            }


        }





        /// <summary>
        ///发送 GET 请求（HTTP），带输入数据
        /// </summary>
        /// <param name="url">地址</param>
        /// <param name="parameters">查询参数集合</param>
        /// <param name="headers">头</param>
        /// <returns></returns>
        public static HttpWebResponse DoGetHttpResponse(string url, IDictionary<string, string> parameters, IDictionary<string, string> headers)
        {

            if (parameters != null && parameters.Count >= 1)
            {

                StringBuilder buffer = new StringBuilder();

                bool first = true;

                foreach (string key in parameters.Keys)
                {
                    if (!first)
                    {
                        buffer.AppendFormat("&{0}={1}", key, parameters[key]);
                    }
                    else
                    {
                        buffer.AppendFormat("?{0}={1}", key, parameters[key]);
                        first = false;
                    }
                }


                url += buffer.ToString();
            }


            //  Debug.WriteLine("发送 GET 请求（HTTP），带输入数据" + url);

            return DoGetHttpResponse(url, headers);

        }




        /// <summary>
        /// 发送http Get请求
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static HttpWebResponse DoGetHttpResponse(string url)
        {
            return DoGetHttpResponse(url, new Dictionary<string, string>(), new Dictionary<string, string>());
        }


        /// <summary>
        /// 发送http Get请求
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <returns></returns>
        public static HttpWebResponse DoGetHttpResponse(string url, IDictionary<String, String> headers)
        {

            int httpStatusCode = 200;

            try
            {

                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

                if (request == null)
                {
                    throw new CWMNetworkException("网络请求发起失败");
                }


                request.Method = "GET";
                request.ContentType = "application/x-www-form-urlencoded"; //链接类型
                if (!(headers == null || headers.Count == 0))
                {

                    foreach (string key in headers.Keys)
                    {
                        request.Headers[key] = headers[key];
                    }

                }

                HttpWebResponse rsp = request.GetResponse() as HttpWebResponse;

                if (rsp == null)
                {
                    throw new CWMNetworkException("网络请求异常");
                }

                httpStatusCode = (int)rsp.StatusCode;

                if (httpStatusCode / 100 == 2)
                {
                    return rsp;
                }

                throw new CWMNetworkException("网络请求状态异常");

            }
            catch (Exception ex)
            {
                Logs.CWMLogs.WriteLog("网络请求异常", ex);
                throw new CWMNetworkException(ex.Message);
            }

        }


        /// <summary>
        /// 从HttpWebResponse对象中提取响应的数据转换为字符串
        /// </summary>
        /// <param name="webresponse"></param>
        /// <returns></returns>
        public static string GetResponseString(HttpWebResponse webresponse)
        {
            using (Stream s = webresponse.GetResponseStream())
            {
                if (s == null)
                {
                    return String.Empty;
                }

                StreamReader reader = new StreamReader(s, Encoding.UTF8);
                return reader.ReadToEnd();
            }
        }


        /// <summary>
        /// 加载图片
        /// 测试图片"https://www.baidu.com/img/bdlogo.png"
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static byte[] DownloadData(string url)
        {
            try
            {

                WebClient client = new WebClient();

                byte[] bytes = client.DownloadData(url);

                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    return ms.ToArray();

                }
            }
            catch (Exception ex)
            {
                Logs.CWMLogs.WriteLog("网络请求异常", ex);
                throw new CWMNetworkException(ex.Message);
            }


        }


        #endregion



    }





}