﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CqingWo.Core.Helper
{
    public class CookieHelper
    {
        [DllImport("Kernel32.dll")]
        private static extern int FormatMessage(int flag, ref IntPtr source, int msgid, int langid, ref string buf, int size, ref IntPtr args);

        [DllImport("wininet.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern bool InternetGetCookieEx(string pchURL, string pchCookieName, StringBuilder pchCookieData, ref UInt32 pcchCookieData, int dwFlags, IntPtr lpReserved);

        [DllImport("wininet.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern int InternetSetCookieEx(string lpszURL, string lpszCookieName, string lpszCookieData, int dwFlags, IntPtr dwReserved);

        public static string GetCookies(string url)
        {
            uint datasize = 256;
            StringBuilder cookieData = new StringBuilder((int)datasize);
            if (!InternetGetCookieEx(url, null, cookieData, ref datasize, 0x40, IntPtr.Zero))
            {
                cookieData = new StringBuilder((int)datasize);
                if (!InternetGetCookieEx(url, null, cookieData, ref datasize, 0x40, IntPtr.Zero))
                    return null;
            }
            return cookieData.ToString();
        }

        public static int SetCookies(string url, string cookieName, string cookieData, DateTime expiresDate)
        {

            cookieData = string.Format("{0}={1};path=/;expires={2}"
                , cookieName
                , cookieData
                , expiresDate.ToString("r"));

            return InternetSetCookieEx(url, null, cookieData, 0x40, IntPtr.Zero);
        }

        /// <summary>
        /// 获取错误提示
        /// </summary>
        /// <returns></returns>
        public static string GetErrMsg()
        {
            var code = Marshal.GetLastWin32Error();
            var tempptr = IntPtr.Zero;
            string msg = null;
            FormatMessage(0x1300, ref tempptr, code, 0, ref msg, 255, ref tempptr);
            return msg;
        }


    }
}
