﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CqingWo.Core
{

    [Serializable]
    public class Message2Info : INotifyPropertyChanged
    {



        /// <summary>
        /// 消息Id
        /// </summary>
        public int MsgId { get; set; } = 0;



        /// <summary>
        /// uid
        /// </summary>
        public string Uid { get; set; } = "";



        /// <summary>
        /// 单位编号
        /// </summary>
        public string UnitCode { get; set; } = "";

        /// <summary>
        /// 设备id
        /// </summary>
        public string DeviceSN { get; set; } = "";

        /// <summary>
        /// 产品id
        /// </summary>
        public int ProductId { get; set; } = 0;

        /// <summary>
        /// 产品类型
        /// </summary>
        public int ProductType { get; set; } = 0;

        /// <summary>
        /// 消息协议
        /// </summary>
        public string Protocol { get; set; } = "";


        /// <summary>
        /// 消息等级
        /// </summary>
        public int MsgLevel { get; set; } = -1;


        /// <summary>
        /// 协议描述
        /// </summary>
        public string Protodesc { get; set; } = "";

        /// <summary>
        /// token
        /// </summary>
        public string Token { get; set; } = "";


        /// <summary>
        /// 报事ip
        /// </summary>
        public string IP { get; set; } = "";




        /// <summary>
        /// 消息分类
        /// </summary>
        public int Type { get; set; } = 0;


        /// <summary>
        /// 消息状态
        /// </summary>
        public int State { get; set; } = 0;

        /// <summary>
        /// 消息说明
        /// </summary>
        public string Message { get; set; } = "";

        /// <summary>
        /// 消息内容
        /// </summary>
        public string Content { get; set; } = "";


        /// <summary>
        /// 缩略图
        /// </summary>
        public string Litpic { get; set; } = "";


        /// <summary>
        /// 设备名称
        /// </summary>
        public string DeviceName { get; set; } = "";


        /// <summary>
        /// 设备编码
        /// </summary>
        public string DeviceCode { get; set; } = "";

        /// <summary>
        /// 时间戳
        /// </summary>
        public int Timestamp = UnixTimeHelper.GetUnixTimeStamp();

        /// <summary>
        /// 失效消息
        /// </summary>
        public int Lose { get; set; } = 0;


        protected internal virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        public event PropertyChangedEventHandler PropertyChanged;


    }
}
