﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using CqingWo.Core.Annotations;

namespace CqingWo.Core.Domain.Message
{
    [Serializable]
    public class MessageRecordInfo : INotifyPropertyChanged
    {

        /// <summary>
        ///  记录Id
        /// </summary>
        public int RecordId { get; set; } = -1;

        /// <summary>
        ///  消息id
        /// </summary>
        public int MsgId { get; set; } = -1;

        /// <summary>
        ///  token
        /// </summary>
        public string Token { get; set; } = "";

        /// <summary>
        ///  设备id
        /// </summary>
        public string DeviceSN { get; set; } = "";

        /// <summary>
        ///  协议关键词
        /// </summary>
        public string Protocol { get; set; } = "";

        /// <summary>
        ///  消息等级
        /// </summary>

        public int MsgLevel { get; set; } = -1;


        /// <summary>
        ///  协议描述
        /// </summary>
        public string Protodesc { get; set; } = "";

        /// <summary>
        ///  关键词
        /// </summary>
        public string Key { get; set; } = "";

        /// <summary>
        ///  参数值
        /// </summary>
        public string Value { get; set; } = "";


        public string Desc { get; set; } = "";

        /// <summary>
        ///  时间戳
        /// </summary>
        private int AddTime { get; set; } = UnixTimeHelper.GetUnixTimeStamp();


        protected internal virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public int Id { get; set; }
    }
}
