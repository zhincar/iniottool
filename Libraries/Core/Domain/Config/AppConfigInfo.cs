﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CqingWo.Core
{

    /// <summary>
    /// APP配置
    /// </summary>
    [Serializable]
    public class AppConfigInfo
    {

        /// <summary>
        /// 应用名称
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// ApiKey
        /// </summary>
        public string ApiKey { get; set; }

        /// <summary>
        /// ApiSecret
        /// </summary>
        public string ApiSecret { get; set; }


        /// <summary>
        /// HttpUrl
        /// </summary>
        public string HttpUrl { get; set; }

    }
}
