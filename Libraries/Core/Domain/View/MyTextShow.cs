﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using CqingWo.Core.Annotations;

namespace CqingWo.Core.Domain.View
{
    public class MyTextShow : INotifyPropertyChanged
    {
        private string _show;

        public event PropertyChangedEventHandler PropertyChanged;

        public string Show
        {
            get { return _show; }
            set
            {
                _show = value;

                // 通知UI，绑定的值发生了改变
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Show"));
            }
        }

        public int Id { set; get; }

    }
}