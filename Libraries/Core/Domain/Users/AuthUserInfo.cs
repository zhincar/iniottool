﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CqingWo.Core
{

    /// <summary>
    /// 权限用户
    /// </summary>
    [Serializable]
    public class AuthUserInfo
    {

        /// <summary>
        /// 登录类型
        /// </summary>
        public string LoginType { get; set; }

        /// <summary>
        /// 用户token
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// uid
        /// </summary>
        public string Uid { get; set; }

        /// <summary>
        /// 用户
        /// </summary>
        public PartUserInfo UserInfo { get; set; }

    }
}
