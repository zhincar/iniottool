﻿
using System;

namespace CqingWo.Core
{
    /// <summary>
    /// 局部用户中心
    /// </summary>
    [Serializable]
    public class PartUserInfo
    {

        /// <summary>
        /// Uid
        /// </summary>
        public string Uid { get; set; }


        /// <summary>
        /// 手机
        /// </summary>
        public string Mobile { get; set; }


        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }


        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }

    }
}
