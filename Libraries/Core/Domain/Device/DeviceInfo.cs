﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CqingWo.Core
{

    /// <summary>
    /// 设备信息
    /// </summary>
    [Serializable]
    public class DeviceInfo : INotifyPropertyChanged
    {

        /// <summary>
        ///设备编号
        /// </summary>
        private string _devicesn = "";

        public DeviceInfo(string deviceSN)
        {
            this._devicesn = deviceSN;
        }

        /// <summary>
        /// 设备编号
        /// </summary>
        public string DeviceSN
        {
            get { return _devicesn; }
            set
            {
                _devicesn = value;
                //激发事件
                if (this.PropertyChanged != null)
                {
                    OnPropertyChanged("DeviceSN");
                }
            }
        }

        /// <summary>
        /// 产品Id
        /// </summary>
        public int ProductId { get; set; } = 0;

        /// <summary>
        /// 单位编码
        /// </summary>
        public string UnitCode { get; set; } = "";

        /// <summary>
        /// 设备类型
        /// </summary>
        public int ProductType { get; set; } = 0;

        /// <summary>
        /// 纬度
        /// </summary>
        public double Latitude { get; set; } = 0.00d;

        /// <summary>
        /// 经度
        /// </summary>
        public double Longitude { get; set; } = 0.00d;

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; } = "";


        /// <summary>
        /// 设备地址
        /// </summary>
        public string Address { get; set; } = "";


        /// <summary>
        /// 设备描述
        /// </summary>
        public string Description { get; set; } = "";

        protected internal virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }


        public event PropertyChangedEventHandler PropertyChanged;

    }
}
