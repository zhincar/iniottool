﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CqingWo.Core;
using CqingWo.RDBSStrategy.SqlServer.model;
using Newtonsoft.Json;

namespace CqingWo.RDBSStrategy.SqlServer
{
    public partial class RdbsStrategy : IRdbsStrategy
    {
        /// <summary>
        /// 消息列表
        /// </summary>
        /// <param name="pageSize">读取条数</param>
        /// <param name="pageNumber">当前页数</param>
        /// <param name="startTime">开始时间</param>
        /// <param name="startMsgId">开始消息Id</param>
        /// <param name="deviceList">设备编号列表</param>
        /// <returns></returns>
        public List<Message2Info> MessageList(int pageSize, int pageNumber, int startTime, int startMsgId, List<string> deviceList)
        {

            try
            {


                Dictionary<string, object> params2 = new Dictionary<string, object>();

                params2.Add("pageSize", pageSize);
                params2.Add("pageNumber", pageNumber);
                params2.Add("startTime", startTime);
                params2.Add("startMsgId", startMsgId);

                StringBuilder sb = new StringBuilder();


                params2.Add("deviceList", AssemblyArray(deviceList));


                MessageInfo messageInfo = DoApi("message/list", params2);

#if DEBUG

                Console.WriteLine("messageInfo:" + messageInfo);

#endif
                if (messageInfo == null)
                {
                    throw new CWMDeserializeException("数据解析失败");
                }


                if (!(messageInfo.State.Equals(0)))
                {
                    throw new CWMFailException(messageInfo.Message);
                }



                List<Message2Info> message2List =
                    (List<Message2Info>)JsonConvert.DeserializeObject(messageInfo.Content, typeof(List<Message2Info>));

                if (message2List == null)
                {
                    throw new CWMDeserializeException("消息数据解析失败");
                }

                return message2List;



            }
            catch (CWMDeserializeException)
            {
                throw;
            }
            catch (CWMFailException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Logs.CWMLogs.WriteLog("数据解析失败", ex);
                throw new CWMDeserializeException("数据解析失败");

            }
        }



        /// <summary>
        /// 消息详情
        /// </summary>
        /// <param name="msgId">消息编号</param>
        /// <returns></returns>
        public void MessageShow(int msgId)
        {

            try
            {


                Dictionary<string, object> params2 = new Dictionary<string, object>();

                params2.Add("msgId", msgId);

                MessageInfo messageInfo = DoApi("message/show", params2);

#if DEBUG

                Console.WriteLine("messageInfo:" + messageInfo);

#endif
                if (messageInfo == null)
                {
                    throw new CWMDeserializeException("数据解析失败");
                }


                if (!(messageInfo.State.Equals(0)))
                {
                    throw new CWMFailException(messageInfo.Message);
                }


                MessageShowModel model =
                    (MessageShowModel) JsonConvert.DeserializeObject(messageInfo.Content, typeof(MessageShowModel));

                if (model == null)
                {
                    throw new CWMDeserializeException("消息数据解析失败");
                }

                throw new CWMMessageSuccessExcption(model.MsgId, model.MessageInfo, model.RecordInfoList, "消息解析成功");




            }
            catch (CWMMessageSuccessExcption)
            {
                throw;
            }
            catch (CWMDeserializeException)
            {
                throw;
            }
            catch (CWMFailException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Logs.CWMLogs.WriteLog("数据解析失败", ex);
                throw new CWMDeserializeException("数据解析失败");

            }


        }
    }
}