﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CqingWo.Core;
using Newtonsoft.Json;

namespace CqingWo.RDBSStrategy.SqlServer
{
    public partial class RdbsStrategy : IRdbsStrategy
    {
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="account">账号/邮箱/手机</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        public void Login(string account, string password)
        {

            try
            {


                Dictionary<string, object> params2 = new Dictionary<string, object>();

                params2.Add("account", account);
                params2.Add("password", password);

                MessageInfo messageInfo = DoApi("account/login", params2);

#if DEBUG

                Console.WriteLine("messageInfo:" + messageInfo);

#endif
                if (messageInfo == null)
                {
                    throw new CWMDeserializeException("数据解析失败");
                }


                if (!(messageInfo.State.Equals(0)))
                {
                    throw new CWMLoginFailExcption(messageInfo.Message);
                }


                AuthUserInfo authUser = (AuthUserInfo)JsonConvert.DeserializeObject(messageInfo.Content, typeof(AuthUserInfo));

                if (authUser == null)
                {
                    throw new CWMDeserializeException("用户数据解析失败");
                }

                throw new CWMLoginSuccessExcption(authUser, "用户登录成功");



            }
            catch (CWMDeserializeException)
            {
                throw;
            }
            catch (CWMLoginFailExcption)
            {
                throw;
            }
            catch (CWMLoginSuccessExcption)
            {
                throw;
            }
            catch (Exception ex)
            {
                Logs.CWMLogs.WriteLog("数据解析失败", ex);
                throw new CWMDeserializeException("数据解析失败");

            }
        }

    }
}
