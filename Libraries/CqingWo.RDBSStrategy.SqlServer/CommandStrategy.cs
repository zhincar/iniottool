﻿using System;
using System.Collections.Generic;
using CqingWo.Core;

namespace CqingWo.RDBSStrategy.SqlServer
{
    public partial class RdbsStrategy : IRdbsStrategy
    {
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="command">账号/邮箱/手机</param>
        /// <param name="deviceList">设备编号列表</param>
        /// <returns></returns>
        public bool SendCommand(string command, List<string> deviceList)
        {

            try
            {


                Dictionary<string, object> params2 = new Dictionary<string, object>();

                params2.Add("commandContent", command);

                params2.Add("deviceList", AssemblyArray(deviceList));


                MessageInfo messageInfo = DoApi("command/send", params2);

#if DEBUG

                Console.WriteLine("messageInfo:" + messageInfo);

#endif
                if (messageInfo == null)
                {
                    throw new CWMDeserializeException("数据解析失败");
                }


                if (!(messageInfo.State.Equals(0)))
                {
                    throw new CWMLoginFailExcption(messageInfo.Message);
                }


                return true;



            }
            catch (CWMDeserializeException)
            {
                throw;
            }
            catch (CWMLoginFailExcption)
            {
                throw;
            }
            catch (CWMLoginSuccessExcption)
            {
                throw;
            }
            catch (Exception ex)
            {
                Logs.CWMLogs.WriteLog("数据解析失败", ex);
                throw new CWMDeserializeException("数据解析失败");

            }
        }

    }
}
