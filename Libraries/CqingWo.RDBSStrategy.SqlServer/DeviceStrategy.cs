﻿using System;
using System.Collections.Generic;
using CqingWo.Core;
using Newtonsoft.Json;

namespace CqingWo.RDBSStrategy.SqlServer
{
    public partial class RdbsStrategy : IRdbsStrategy
    {
     

        /// <summary>
        /// 设备列表
        /// </summary>
        /// <param name="pageSize">条数</param>
        /// <param name="pageNumber">当前页数</param>
        /// <returns></returns>
        public List<DeviceInfo> DeviceList(int pageSize,int pageNumber)
        {

            try
            {

                Dictionary<string, object> params2 = new Dictionary<string, object>();

                params2.Add("pageSize", pageSize);
                params2.Add("pageNumber", pageNumber);

                MessageInfo messageInfo = DoApi("device/list", params2);

#if DEBUG

                Console.WriteLine("messageInfo:" + messageInfo);

#endif

                if (messageInfo == null)
                {
                    throw new CWMDeserializeException("数据解析失败");
                }

                if (!(messageInfo.State.Equals(0)))
                {
                    throw new CWMFailException(messageInfo.Message);
                }



                List<DeviceInfo> deviceInfoList = (List<DeviceInfo>)JsonConvert.DeserializeObject(messageInfo.Content, typeof(List<DeviceInfo>));

                if (deviceInfoList == null)
                {
                    throw new CWMFailException("数据解析失败");
                }

                return deviceInfoList;
            }
            catch (CWMDeserializeException)
            {
                throw;
            }
            catch (CWMFailException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Logs.CWMLogs.WriteLog("数据解析失败", ex);
                throw new CWMFailException("数据解析失败");

            }

        }

        /// <summary>
        /// 查询设备信息
        /// </summary>
        /// <param name="deviceSN">设备编号</param>
        /// <returns></returns>
        public DeviceInfo DeviceShow(string deviceSN)
        {
            try
            {

                Dictionary<string, object> params2 = new Dictionary<string, object>();

                params2.Add("deviceSN", deviceSN);

                MessageInfo messageInfo = DoApi("device/show", params2);

#if DEBUG

                Console.WriteLine("messageInfo:" + messageInfo);

#endif

                if (messageInfo == null)
                {
                    throw new CWMDeserializeException("数据解析失败");
                }

                if (!(messageInfo.State.Equals(0)))
                {
                    throw new CWMFailException(messageInfo.Message);
                }



                DeviceInfo deviceInfo = (DeviceInfo)JsonConvert.DeserializeObject(messageInfo.Content, typeof(DeviceInfo));

                if (deviceInfo == null)
                {
                    throw new CWMFailException("数据解析失败");
                }

                return deviceInfo;
            }
            catch (CWMDeserializeException)
            {
                throw;
            }
            catch (CWMFailException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Logs.CWMLogs.WriteLog("数据解析失败", ex);
                throw new CWMFailException("数据解析失败");

            }
        }
    }
}
