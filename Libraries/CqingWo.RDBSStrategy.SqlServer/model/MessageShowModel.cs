﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CqingWo.Core;
using CqingWo.Core.Domain.Message;

namespace CqingWo.RDBSStrategy.SqlServer.model
{
    public class MessageShowModel
    {

        /// <summary>
        ///消息id
        /// </summary>
        public int MsgId { get; set; } = 0;

        /// <summary>
        /// 消息
        /// </summary>
        public Message2Info MessageInfo { get; set; }

        /// <summary>
        /// 解析记录
        /// </summary>
        public List<MessageRecordInfo> RecordInfoList { get; set; }
    }
}
