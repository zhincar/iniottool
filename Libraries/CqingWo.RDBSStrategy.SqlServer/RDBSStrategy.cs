﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using CqingWo.Core;
using CqingWo.Services;
using Newtonsoft.Json;

namespace CqingWo.RDBSStrategy.SqlServer
{
    public partial class RdbsStrategy : IRdbsStrategy
    {



        public string RunSql(string sql)
        {
            return "hello";
        }


        /// <summary>
        /// get
        /// </summary>
        /// <param name="methodUrl">地址</param>
        /// <param name="parameters">参数</param>
        /// <returns></returns>
        public string DoGet(string methodUrl, Dictionary<string, string> parameters = null)
        {
            try
            {

                String httpUrl = GetApiUrl(methodUrl);

                HttpWebResponse response = HttpHelper.DoGetHttpResponse(httpUrl, parameters, GetApiHeader());


                if (response.StatusCode == HttpStatusCode.OK)
                {

                    return HttpHelper.GetResponseString(response);

                }
            }
            catch (Exception ex)
            {
                Logs.CWMLogs.WriteLog("网络GET请求", ex);

                throw ex;
            }

            throw new CWMNetworkException("网络异常");

        }




        /// <summary>
        /// Post
        /// </summary>
        /// <param name="methodUrl">地址</param>
        /// <param name="parameters">参数</param>
        /// <returns></returns>
        private string DoPost(string methodUrl, Dictionary<string, object> parameters = null)
        {

            try
            {
                String httpUrl = GetApiUrl(methodUrl);

                HttpWebResponse response = HttpHelper.DoPostHttpResponse(httpUrl, parameters, GetApiHeader());

                if (response.StatusCode == HttpStatusCode.OK)
                {

                    return HttpHelper.GetResponseString(response);

                }

            }
            catch (Exception ex)
            {

                throw ex;
            }


            throw new CWMNetworkException("网络异常");
        }


        /// <summary>
        /// 拼装API URL
        /// </summary>
        /// <param name="methodUrl">地址</param>
        /// <returns></returns>
        private String GetApiUrl(string methodUrl)
        {
            return ConfigUtlis.GetInstance().GetAppConfigInfo().HttpUrl + methodUrl;
        }

        /// <summary>
        /// 拼装头文件
        /// </summary>
        /// <returns></returns>
        private IDictionary<string, string> GetApiHeader()
        {

            IDictionary<String, String> headers = new Dictionary<string, string>();

            headers.Add("X-CWMAPI-ApiKey", ConfigUtlis.GetInstance().GetAppConfigInfo().ApiKey);
            headers.Add("X-CWMAPI-ApiSecret", ConfigUtlis.GetInstance().GetAppConfigInfo().ApiSecret);

            return headers;
        }


        /// <summary>
        /// 调用接口
        /// </summary>
        /// <param name="methodUrl"></param>
        /// <param name="params2"></param>
        /// <returns></returns>
        public MessageInfo DoApi(string methodUrl, Dictionary<string, object> params2)
        {

            try
            {

                string jsonStr = DoPost(methodUrl, params2);


                MessageInfo messageInfo = JsonConvert.DeserializeObject<MessageInfo>(jsonStr);

                if (messageInfo == null)
                {
                    throw new CWMDeserializeException("数据解析失败");
                }

                return messageInfo;

            }
            catch (Exception ex)
            {

                Logs.CWMLogs.WriteLog("调用接口", ex);
                throw;

            }

        }


        /// <summary>
        /// 拼装数组
        /// </summary>
        /// <param name="sList"></param>
        /// <param name="separator">分隔符</param>
        /// <returns></returns>
        public string AssemblyArray(List<string> sList, char separator = ',')
        {

            try
            {
                StringBuilder sb = new StringBuilder();

                foreach (string s in sList)
                {

                    sb.AppendFormat("{0}{1}", separator, s);

                }

                if (sb.Length >= 1)
                {
                    sb.Remove(0, 1);

                }

                return sb.ToString();

            }
            catch (Exception)
            {

            }

            return "";

        }


    }
}
