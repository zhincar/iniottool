﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CqingWo.Core;
using CqingWo.Logs;

namespace CqingWo.ConfigStrategy.File
{
    public class ConfigStrategy : IConfigStrategy
    {



        /// <summary>
        /// config
        /// </summary>
        private readonly System.Configuration.Configuration _configuration = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);




        /// <summary>
        /// 测试
        /// </summary>
        public void Test()
        {
            CqingWo.Logs.CWMLogs.WriteLog("周婷,我喜欢你");
        }


        /// <summary>
        /// 保存参数
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">值</param>
        public bool Save(string key, string value)
        {

            try
            {
                _configuration.AppSettings.Settings[key].Value = value;

                _configuration.Save(System.Configuration.ConfigurationSaveMode.Modified);
                System.Configuration.ConfigurationManager.RefreshSection("appSettings");

                return true;

            }
            catch (Exception ex)
            {
                CWMLogs.WriteLog("保存参数失败", ex);
            }

            return false;

        }


        /// <summary>
        /// 保存多个参数
        /// </summary>
        /// <param name="params2">参数</param>
        /// <returns></returns>
        public bool Save(IDictionary<string, string> params2)
        {
            try
            {
                foreach (string key in params2.Keys)
                {
                    _configuration.AppSettings.Settings[key].Value = params2[key] ?? "";

                }

                _configuration.Save(System.Configuration.ConfigurationSaveMode.Modified);
                System.Configuration.ConfigurationManager.RefreshSection("appSettings");

                return true;

            }
            catch (Exception ex)
            {
                CWMLogs.WriteLog("保存参数失败", ex);
            }

            return false;
        }


        /// <summary>
        /// 读取参数
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="defaultValue">默认值</param>
        public string Read(string key, string defaultValue = "")
        {

            try
            {
                return _configuration.AppSettings.Settings[key].Value ?? defaultValue;

            }
            catch (Exception ex)
            {
                CWMLogs.WriteLog("读取参数失败", ex);
            }

            return defaultValue;
        }


    }
}
