﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CqingWo.Core;
using CqingWo.Services;
using Newtonsoft.Json;

namespace CqingWo.Application.application
{
    /// <summary>
    /// WndDeviceAdd.xaml 的交互逻辑
    /// </summary>
    public partial class WndDeviceManage : Window
    {

        private static readonly object Locker = new object();

        private ObservableCollection<DeviceInfo> _unCheckList = new ObservableCollection<DeviceInfo>();

        public WndDeviceManage()
        {
            Init();
        }

        public WndDeviceManage(double x, double y)
        {

            //启用‘Manual’属性后，可以手动设置窗体的显示位置
            this.WindowStartupLocation = WindowStartupLocation.Manual;

            this.Top = x;
            this.Left = y;

            Init();
        }

        private void Init()
        {
            InitializeComponent();


            this.SelectionListBox.ItemsSource = App.DeviceInfoList;

            this.UncheckedListBox.ItemsSource = _unCheckList;

            this.InputBox.Focus();


        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {


            lock (Locker)
            {

              

                string deviceSN = this.InputBox.Text.Trim();

                if (string.IsNullOrWhiteSpace(deviceSN))
                {

                    Show("设备编号不能为空");
                    return;
                }

                if (App.DeviceInfoList == null)
                {
                    App.DeviceInfoList = new ObservableCollection<DeviceInfo>();
                }



                foreach (DeviceInfo info in _unCheckList)
                {

                    if (info.DeviceSN.Contains(deviceSN))
                    {
                        Show("重复的设备");

                        this.InputBox.Text = "";
                        return;
                    }

                }

                foreach (DeviceInfo info in App.DeviceInfoList)
                {
                    if (info.DeviceSN.Equals(deviceSN))
                    {
                        Show("重复的设备");

                        this.InputBox.Text = "";
                        return;
                    }
                }

                DeviceInfo deviceInfo = Devices.Instance.Show(deviceSN);

                if (deviceInfo == null)
                {
                    Show("不正确的设备");
                    return;
                }


                App.DeviceInfoList.Add(deviceInfo);


                //App.DeviceInfoList.Add(new DeviceInfo(deviceSN));



                this.InputBox.Text = "";

            }


            //this.UncheckedListBox.Items.Add(deviceSN);



        }


        private void Unchecked_Button_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            lock (Locker)
            {
                try
                {

                    Button button = sender as Button;

                    if (button == null)
                    {
                        Show("设备异常");
                        return;
                    }

                    DeviceInfo deviceInfo = (DeviceInfo)button.DataContext;

                    foreach (DeviceInfo info in _unCheckList)
                    {
                        if (info.DeviceSN.Equals(deviceInfo.DeviceSN))
                        {
                            App.DeviceInfoList.Add(info);
                            _unCheckList.Remove(info);
                            return;
                        }
                    }






                }
                catch (Exception ex)
                {
                    Logs.CWMLogs.WriteLog("设备异常", ex);
                }
            }

        }

        private void Selection_Button_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            lock (Locker)
            {
                try
                {

                    Button button = sender as Button;

                    if (button == null)
                    {
                        Show("设备异常");
                        return;
                    }

                    DeviceInfo deviceInfo = (DeviceInfo)button.DataContext;

                    if (deviceInfo == null)
                    {
                        Show("设备信息异常");
                        return;
                    }

                    foreach (DeviceInfo info in App.DeviceInfoList)
                    {
                        if (deviceInfo.DeviceSN.Equals(info.DeviceSN))
                        {
                            _unCheckList.Add(deviceInfo);
                            App.DeviceInfoList.Remove(deviceInfo);
                            break;

                        }
                    }



                }
                catch (Exception ex)
                {
                    Logs.CWMLogs.WriteLog("设备信息异常", ex);
                }
            }

        }


        /// <summary>
        /// 消息显示
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="title">标题</param>
        public void Show(string message, string title = "设备管理")
        {

            bool isShowMessage = this.IsShowMessageCheckBox.IsChecked ?? false;

            if (isShowMessage)
            {
                MessageBox.Show(message, title);
            }
        }
    }
}
