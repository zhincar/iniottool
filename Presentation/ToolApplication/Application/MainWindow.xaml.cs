﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using CqingWo.Core;
using CqingWo.Services;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Threading;
using CqingWo.Application.Application;
using CqingWo.Application.Model;

namespace CqingWo.Application.application
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {

        public PartUserInfo CurrentUserInfo { private get; set; }

        /// <summary>
        /// 定时器
        /// </summary>
        public DispatcherTimer DispatcherTimer;


        public ObservableCollection<MessageBindModel> Message2InfoList { get; set; } = new ObservableCollection<MessageBindModel>();


        public MainWindow()
        {

            Init();

        }


        public MainWindow(double x, double y)
        {
            //启用‘Manual’属性后，可以手动设置窗体的显示位置
            this.WindowStartupLocation = WindowStartupLocation.Manual;
            this.Top = x;
            this.Left = y;
            Init();

        }

        private void Init()
        {

            InitializeComponent();


            this.DeviceListBox.ItemsSource = App.DeviceInfoList;
            this.DeviceListBox.SetBinding(TextBox.TextProperty, new Binding("SelectedItem.Id") { Source = DeviceListBox });

            this.MessageListBox.ItemsSource = Message2InfoList;
            this.MessageListBox.SetBinding(TextBox.TextProperty, new Binding("SelectedItem.Id") { Source = MessageListBox });



            DispatcherTimer = new DispatcherTimer();
            //定时查询-定时器
            DispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            DispatcherTimer.Interval = new TimeSpan(0, 0, 10);
            DispatcherTimer.Start();

            Console.Out.WriteLine("定时器启动成功");


        }


        private void MenuOpen_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Open the file you want.");
        }

        private void DeviceManage_Click(object sender, RoutedEventArgs e)
        {

            WndDeviceManage wndDevice = new WndDeviceManage(this.Top, this.Left);

            wndDevice.Show();


        }

        /// <summary>
        /// 清空设备
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeviceManage_Clear_Click(object sender, RoutedEventArgs e)
        {

            App.DeviceInfoList.Clear();


        }
        private void Helper_Click(object sender, RoutedEventArgs e)
        {
            App.Show("帮助手册");


        }


        /// <summary>
        /// 远程配置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandSetting_Click(object sender, RoutedEventArgs e)
        {
            string command = "";

            Task<bool> task = Task.Run(() => Commands.Instance.Send(command, App.DeviceInfoList));

        }

        /// <summary>
        /// 发送命令
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            string command = this.CommandBox.Text.Trim();


            if (string.IsNullOrWhiteSpace(command))
            {
                MessageBox.Show("命令不能为空");
                return;
            }

            this.CommandBox.Text = string.Empty;

            Task<bool> task = Task.Run(() => Commands.Instance.Send(command, App.DeviceInfoList));

            if (task.Result)
            {
                MessageBox.Show("命令发送成功");
            }
            else
            {
                MessageBox.Show("命令发送失败");
            }


        }

        /// <summary>
        /// 定时器回调函数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            RefreshList();
        }


        /// <summary>
        /// 刷新消息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MessageRefresh_Click(object sender, RoutedEventArgs e)
        {
            RefreshList();
        }


        /// <summary>
        /// 刷新消息
        /// </summary>
        public void RefreshList()
        {

            if (App.DeviceInfoList.Count <= 0)
            {
                return;
            }

            List<Message2Info> message2InfoList = Messages.Instance.List(10, 1, App.StartTime, App.StartMsgId, App.DeviceInfoList);


            if (message2InfoList == null || message2InfoList.Count <= 0)
            {
                return;

            }

            foreach (Message2Info info in message2InfoList)
            {

                Message2InfoList.Add(new MessageBindModel(info));

                if (info.MsgId >= App.StartMsgId)
                {
                    App.StartMsgId = info.MsgId;
                }
            }


            MessageListBox.ScrollIntoView(MessageListBox.Items[MessageListBox.Items.Count - 1]);



            Console.Out.WriteLine("定时器处理");
        }


        /// <summary>
        /// 详情消息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MessageListBox_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
            
                StackPanel panel = sender as StackPanel;

                // ReSharper disable once UseNullPropagation
                if (panel == null)
                {
                    return; ;
                }

                MessageBindModel model = (MessageBindModel)panel.DataContext;



                if (model == null)
                {
                    return;
                }

             

                WndMessageShow wndMessageShow = new WndMessageShow(model, this.Top, this.Left);

                wndMessageShow.ShowDialog();



            }
            catch (Exception ex)
            {

                Logs.CWMLogs.WriteLog("", ex);

            }


        }

        private void DeviceBtn_Click(object sender, RoutedEventArgs e)
        {

            WndDeviceManage wndDevice = new WndDeviceManage(this.Top, this.Left);

            wndDevice.Show();
        }

        private void RefreshBtn_Click(object sender, RoutedEventArgs e)
        {
            RefreshList();

        }

        private void SettingBtn_Click(object sender, RoutedEventArgs e)
        {
            string command = "";

            Task<bool> task = Task.Run(() => Commands.Instance.Send(command, App.DeviceInfoList));

        }
    }
}
