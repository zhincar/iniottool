﻿using System;
using System.Threading.Tasks;
using System.Windows;
using CqingWo.Core;
using CqingWo.Services;

namespace CqingWo.Application.application
{
    /// <summary>
    /// WndSetting.xaml 的交互逻辑
    /// </summary>
    public partial class WndSetting : Window
    {

        public WndSetting()
        {
            Init();
        }

        public WndSetting(double x, double y)
        {
            //启用‘Manual’属性后，可以手动设置窗体的显示位置
            this.WindowStartupLocation = WindowStartupLocation.Manual;
            this.Top = x;
            this.Left = y;
            Init();

        }

        private void Init()
        {

            InitializeComponent();

            ApiKeyBox.Text = ConfigUtlis.GetInstance().GetAppConfigInfo().ApiKey;
            ApiSecretBox.Password = ConfigUtlis.GetInstance().GetAppConfigInfo().ApiSecret;
            HttpUrlBox.Text = ConfigUtlis.GetInstance().GetAppConfigInfo().HttpUrl;
        }

        private void LoginBtn_Click(object sender, RoutedEventArgs e)
        {

            AppConfigInfo configInfo = new AppConfigInfo();

            configInfo.ApiKey = ApiKeyBox.Text.Trim();
            configInfo.ApiSecret = ApiSecretBox.Password.Trim();
            configInfo.HttpUrl = HttpUrlBox.Text.Trim();

            if (ConfigUtlis.GetInstance().SaveAppConfigInfo(configInfo))
            {
                MessageBox.Show("参数保存成功");
                Close();
            }
            else
            {
                MessageBox.Show("参数保存失败");
            }

       


        }



    }
}
