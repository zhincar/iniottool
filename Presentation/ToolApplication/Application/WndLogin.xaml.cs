﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using CqingWo.Core;
using CqingWo.Services;
using Newtonsoft.Json;

namespace CqingWo.Application.application
{
    /// <summary>
    /// WndLogin.xaml 的交互逻辑
    /// </summary>
    public partial class WndLogin : Window
    {
        private PartUserInfo _currentUserInfo;






        public PartUserInfo CurrentUserInfo
        {
            get { return _currentUserInfo; }
            set { _currentUserInfo = value; }
        }

        public WndLogin()
        {
            Init();
        }

        public WndLogin(double x, double y)
        {
            //启用‘Manual’属性后，可以手动设置窗体的显示位置
            this.WindowStartupLocation = WindowStartupLocation.Manual;
            this.Top = x;
            this.Left = y;
            Init();
        }

        public void Init()
        {

            InitializeComponent();

        }


        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoginBtn_Click(object sender, RoutedEventArgs e)
        {
            this.AccountBox.IsEnabled = false;
            this.PasswordBox.IsEnabled = false;
            this.LoginBtn.IsEnabled = false;

            //账号
            TextBox accountBox = this.AccountBox;

            //密码
            PasswordBox passwordBox = this.PasswordBox;

            string account = accountBox.Text;

            if (string.IsNullOrWhiteSpace(account))
            {

                MessageBox.Show("用户名不能为空", "登录提示");
                accountBox.Focus();
                return;
            }

            string password = passwordBox.Password;



            if (string.IsNullOrWhiteSpace(password))
            {

                MessageBox.Show("密码不能为空", "登录提示");

                accountBox.Focus();
                return;
            }

            Task<bool> task = Task.Run(() =>
                   {

                       try
                       {
                           Users.Instance.Login(account, password);

                       }
                       catch (CWMLoginSuccessExcption ex)
                       {
                           string token = ex.AuthUserInfo.Token;


                          
                           //this.Close();

                           return true;
                       }
                       catch (Exception ex)
                       {

                           MessageBox.Show(ex.Message, "用户登录失败");

                           return false;
                       }

                       return false;

                   });

            if (task.Result)
            {

                MainWindow mainWindow = new MainWindow();

                System.Windows.Application.Current.MainWindow = mainWindow;

                Close();

                mainWindow.ShowDialog();
            }
            else
            {
                this.AccountBox.IsEnabled = true;
                this.PasswordBox.IsEnabled = true;
                this.LoginBtn.IsEnabled = true;
            }


        }

        /// <summary>
        /// 打开设置界面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingImage_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            WndSetting wndSetting = new WndSetting(this.Top + 45, this.Left + 25);
            wndSetting.ShowDialog();


        }

        private void Code2_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            WndCode2 wndCode2 = new WndCode2(this.Top, this.Left);
            System.Windows.Application.Current.MainWindow = wndCode2;


            Close();

            wndCode2.ShowDialog();
        }

     


    }
}
