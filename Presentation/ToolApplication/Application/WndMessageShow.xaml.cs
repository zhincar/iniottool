﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using System.Windows.Media.Imaging;
using CqingWo.Application.Model;
using CqingWo.Core;
using CqingWo.Core.Domain.Message;
using CqingWo.Core.Domain.View;
using CqingWo.Core.Extend;
using CqingWo.Services;

namespace CqingWo.Application.Application
{
    /// <summary>
    /// MessageShow.xaml 的交互逻辑
    /// </summary>
    public partial class WndMessageShow : Window
    {
        /// <summary>
        ///设备编号绑定源
        /// </summary>
        readonly MyTextShow _myDeviceSnShow = new MyTextShow();

        /// <summary>
        /// 消息Id绑定源
        /// </summary>
        readonly MyTextShow _myMsgIdShow = new MyTextShow();


        /// <summary>
        /// 消息协议绑定源
        /// </summary>
        readonly MyTextShow _myProtocolShow = new MyTextShow();

        /// <summary>
        /// 消息协议描述
        /// </summary>
        readonly MyTextShow _myProtodescShow = new MyTextShow();


        private AsyncObservableCollection<MessageRecordInfo> _recordInfoList { get; set; } = new AsyncObservableCollection<MessageRecordInfo>();

        private WndMessageShow()
        {

        }

        private void Init(MessageBindModel model)
        {
            try
            {
                InitializeComponent();

                if (model == null)
                {
                    MessageBox.Show("消息异常");
                    this.Close();
                    return;
                }

                int msgId = TypeHelper.StringToInt(model.MsgId, 0);

                if (msgId <= 0)
                {

                    MessageBox.Show("消息ID异常");
                    this.Close();
                    return;
                }

                InitBind(msgId, model.DeviceSN, model.Protocol, model.Description);



                Task task = Task.Run(() =>
                {

                    try
                    {
                        Messages.Instance.Show(msgId);
                    }
                    catch (CWMMessageSuccessExcption ex)
                    {
                        if (ex.MessageInfo == null)
                        {
                            MessageBox.Show("消息异常");
                            this.Close();
                        }


                        RefreshUi(ex.MessageInfo, ex.RecordInfoList);

                    }



                });



            }
            catch (Exception ex)
            {

                Logs.CWMLogs.WriteLog("自定义构造方法", ex);

            }

        }
        

        /// <summary>
        /// 绑定数据
        /// </summary>
        /// <param name="msgId"></param>
        /// <param name="deviceSN"></param>
        /// <param name="protocol"></param>
        /// <param name="protodesc"></param>
        private void InitBind(int msgId, string deviceSN, string protocol, string protodesc)
        {

            // 绑定数据
            Binding deviceSnBinding = new Binding();
            deviceSnBinding.Source = _myDeviceSnShow;
            deviceSnBinding.Path = new PropertyPath("Show");
            BindingOperations.SetBinding(DeviceSNTextBox, TextBox.TextProperty, deviceSnBinding);

            _myDeviceSnShow.Show = deviceSN;


            // 绑定数据
            Binding msgIdBinding = new Binding();
            msgIdBinding.Source = _myMsgIdShow;
            msgIdBinding.Path = new PropertyPath("Show");
            BindingOperations.SetBinding(MsgIdTextBox, TextBox.TextProperty, msgIdBinding);


            _myMsgIdShow.Show = msgId.ToString();


            // 绑定数据
            Binding protocolBing = new Binding();
            protocolBing.Source = _myProtocolShow;
            protocolBing.Path = new PropertyPath("Show");
            BindingOperations.SetBinding(ProtocolTextBox, TextBox.TextProperty, protocolBing);


            _myProtocolShow.Show = protocol;


            // 绑定数据
            Binding protdescBing = new Binding();
            protdescBing.Source = _myProtodescShow;
            protdescBing.Path = new PropertyPath("Show");
            BindingOperations.SetBinding(DeviceDescTextBox, TextBox.TextProperty, protdescBing);


            _myProtodescShow.Show = protodesc;

            byte[] btyarray = HttpHelper.DownloadData("https://community.ctwing.cn/uc_server/images/noavatar_middle.gif");

            MemoryStream ms = new MemoryStream(btyarray);

            LitpicImageBox.Source = BitmapFrame.Create(ms, BitmapCreateOptions.None, BitmapCacheOption.Default);



           

            RecordListBox.ItemsSource = _recordInfoList;


        }

        private void RefreshUi(Message2Info message2Info, List<MessageRecordInfo> recordList)
        {

            _myDeviceSnShow.Show = message2Info.DeviceSN;
            _myMsgIdShow.Show = message2Info.MsgId.ToString();
            _myProtocolShow.Show = message2Info.Protocol;
            _myProtodescShow.Show = message2Info.Protodesc;


            foreach (MessageRecordInfo recordInfo in recordList)
            {
#if DEBUG
                Console.WriteLine(recordInfo.Key);
                Console.WriteLine(recordInfo.Value);
#endif

                _recordInfoList.Add(recordInfo);
            }

        }


        /// <summary>
        /// 自定义构造方法
        /// </summary>
        /// <param name="model">model消息</param>
        /// <param name="x">X坐标</param>
        /// <param name="y">Y坐标</param>
        public WndMessageShow(MessageBindModel model, double x = 0, double y = 0)
        {

            this.WindowStartupLocation = WindowStartupLocation.Manual;
            this.Top = x;
            this.Left = y;

            Init(model);




        }




        private void LoginBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }

  
}
