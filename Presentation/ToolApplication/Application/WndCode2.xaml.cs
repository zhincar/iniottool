﻿using System.Windows;

namespace CqingWo.Application.application
{
    /// <summary>
    /// WndCode2.xaml 的交互逻辑
    /// </summary>
    public partial class WndCode2 : Window
    {


        public WndCode2()
        {
            Init();
        }

        public WndCode2(double x, double y)
        {
            //启用‘Manual’属性后，可以手动设置窗体的显示位置
            this.WindowStartupLocation = WindowStartupLocation.Manual;
            this.Top = x;
            this.Left = y;
            Init();

        }



        private void Init()
        {

            InitializeComponent();
            Code2Browser.ObjectForScripting = new ScriptEvent();

          
            Code2Browser.Navigate("https://iot.510link.com/token/login");
        }

        [System.Runtime.InteropServices.ComVisible(true)]
        public class ScriptEvent
        {
            //供JS调用
            public void ShowMessage(string message)
            {
                MessageBox.Show(message);
            }
        }



        private void Code2Window_Closed(object sender, System.EventArgs e)
        {
            WndLogin wndLogin = new WndLogin(this.Top, this.Left);

            System.Windows.Application.Current.MainWindow = wndLogin;


            Close();

            wndLogin.ShowDialog();
        }


    }
}
