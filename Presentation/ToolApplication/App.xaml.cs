﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using CqingWo.Application.application;
using CqingWo.Core;
using MainWindow = CqingWo.Application.application.MainWindow;

namespace CqingWo.Application
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : System.Windows.Application
    {

        private bool _bFirst = true;

        public static ObservableCollection<DeviceInfo> DeviceInfoList { get; set; } = new ObservableCollection<DeviceInfo>();


        private PartUserInfo _userInfo;

        /// <summary>
        ///  启动时间
        /// </summary>
        public static int StartTime { get; } = UnixTimeHelper.GetUnixTimeStamp();

        public static int StartMsgId { get; set; } = 0;


        protected override void OnStartup(StartupEventArgs e)
        {

            if (_bFirst)
            {
                System.Windows.Application.Current.ShutdownMode = System.Windows.ShutdownMode.OnExplicitShutdown;
                WndLogin window = new WndLogin();
                bool? dialogResult = window.ShowDialog();

                if ((dialogResult.HasValue) && (dialogResult.Value))
                {
                    _userInfo = window.CurrentUserInfo;
                    _bFirst = false;

                    OnStartup(e);
                    System.Windows.Application.Current.ShutdownMode = ShutdownMode.OnLastWindowClose;
                }
                else
                {
                    Shutdown();
                }
            }
            else
            {
                MainWindow window = new MainWindow();
                window.CurrentUserInfo = _userInfo;
                window.ShowDialog();
            }
        }

        /// <summary>
        /// 消息显示
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="title"></param>
        public static void Show(string msg, string title = "英卡电子")
        {
            MessageBox.Show(msg, title);
        }

    }

}
