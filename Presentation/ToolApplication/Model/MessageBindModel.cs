﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CqingWo.Core;

namespace CqingWo.Application.Model
{
    public class MessageBindModel : INotifyPropertyChanged
    {

        public MessageBindModel(Message2Info info)
        {

            if (info == null)
            {
                return;
            }

            this.Time = UnixTimeHelper.UnixTimeStampToDateTime(info.Timestamp).ToLongTimeString();
            this.MsgId = info.MsgId.ToString();
            this.DeviceSN = info.DeviceSN;
            this.Protocol = info.Protocol;
            this.Description = info.Protodesc;
        }

        public string MsgId { get; set; } = "0";

        /// <summary>
        /// 时间
        /// </summary>
        public string Time { get; set; } = "";


        /// <summary>
        /// 设备编号
        /// </summary>
        public string DeviceSN { get; set; } = "";


        /// <summary>
        /// 协议
        /// </summary>
        public string Protocol { get; set; } = "";

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; } = "";


        protected internal virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        public event PropertyChangedEventHandler PropertyChanged;


        public int Id { get; set; }

    }
}
